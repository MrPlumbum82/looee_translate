# Looee Translate
## TODO
  - make base UI
    - input box
    - output text (label)
  - make UI responsive
  - write tests
  - add colors or pictures
    - change favicon
    - add looee pictures for background

  - create new gitlab ci that will deploy to your VPS
    - try to deploy from gitlab directly to vps in one of two ways
      - publish an artifact, or set of artifacts on gitlab, versioned... and then deploy the artifact
      - deploy directly to your vps